Naam: abday-münchen bitter mosaic
smaak beschrijving:

datum brouw      | 2021 09 04
dagen vergisten  | XX
dagen lageren    | XX
warme bottel     | 2021 09 15
koude bottel     | jjjj mm dd
proef            | jjjj mm dd

5 u 36 min u  m  duratie  
begin      15 00 verzamelen en nadenken
wegen      15 05 5 min
schroten   15 08 3 min
maischen   15 16 8 min
spoelen    17 46 2,5 uren
kook
     vlaam 18 05 20 min
      kook 18 30 25 min
       hop 18 46 16 min
afkoel     19 46 60 min
24°        19 55  9 min
vergisting 20 04 (in doos)
afwassen   20 05 1 min
eind       20 36 31 min

schroten
ebc    mout         grammen 
40-50  abbey         435
15     münchen       885
totaal granen       1320
totaal vergistbaar  1320

maische
C    t  u  m
40° 05 15 25 - 15 30 nat maken
7 min
52° 10 15 37 - 15 47 eiwitrust (vergisting+schuim)
11 min
62° 50 15 58 - 16 48 versuikering (droger+sterker)
18 min
73° 30 17 06 - 17 36 vervloeiing (zoeter+lichter)
5 min
78° 05 17 41 - 17 46 stoppen

ph 5
liters maisch water 3,5
maisch g/l  =totaal granen / liters maisch water
maisch l/kg =liters maisch water / (totaal granen / 1000)
maisch g/l  377
maisch l/kg 2.4242
spoel water 6,0
tot brix    5

kook
9,2 liters (zo vol moglijk)
hop         procent grammen minuten 
mosaic        12,6%    15    60
saaz          03,5%    12    01
kruiden/stof    grammen minuten 
ultra moss       1      10      

gist safale be-256 (gesprekeld)

refractometer          pre-boil OG
brix                   -oeps-   1,046
specifiek zwartekracht -oeps-   11,4

hydrometer      FG
potentieel alc   1,2
suiker g/l      30
dichtheid     1011

brix 35 = 1046 - 1011
sg  8,4 = 11,4 - 3

procent alcohol
(og - fg) * 0,136 + 0,4  = 5,16
(plato1 - plato0) * 0,544 + 0,4 = 4,9696

bottel
05,8l bier
45,0Xg suiker
  3 X 75,0cl 2,25
 10 X 33,0cl 3,30
555 cl

verwarming vergisting en flessen 
uren vergisting 
kWh vergisting   =(M40-L40)+J46 
uren bottelen 
kWh bottelen 
kost / kWh       € 062 
kost  =(I48+I46)*I49 
