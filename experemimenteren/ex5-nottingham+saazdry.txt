Naam: ex5 - lite amber bitter big nose
smaak beschrijving: jonge bier was echt lekker, gras, droog, niet zo bitter

za 09 apr 2022 17:58:30 biter, neus van gras, hoi, ook zoet en bruisend


datum brouw      | 2022 02 26
dagen vergisten  | 8 (smaak van lelijk gist, misschien te lang?)
datum overhevel  | 2022 03 06
dagen lageren    | 25
warme bottel     | 2022 03 31
koude bottel     | jjjj mm dd
proef            | jjjj mm dd

           u  m  duratie  
begin      11 00
wegen      11 15
schroten   11 30
maischen   12 00
spoelen    14 10
kook
     vlaam 14 55
      kook 15 06
       hop 15 17
afkoel     16 17
vergisting 16 30
afwassen   16 35
eind       17 24

schroten
ebc    mout         grammen 
25-35  pils          900
totaal granen        950
         suiker       20
totaal vergistbaar  0970
40-50  abbey          20 vergeten en toegevoegd op spoel

maische
C    t  u  m
40° 05 12 12 nat maken
52° 08 12 24 eiwitrust (vergisting+schuim)
62° 40 12 41 versuikering (droger+sterker)
73° 25 13 35 vervloeiing (zoeter+lichter)
78° 05 14 04 stoppen
op 62° was klaar, deksel erop en soep maken
- maar vlam was vergeten uit te zetten!
- was klienste op laagste vuur
- gevonden en herstelt maar heeft zeker 65° geraakt

ph 6 @ 62°
ph 6 @ pre-boil

liters maisch water 3,0
maisch g/l  =totaal granen / liters maisch water
maisch l/kg =liters maisch water / (totaal granen / 1000)
maisch g/l  300
maisch l/kg 3,33
spoel water 6 (of misschien 7)
tot brix    4,4 (1,017)

kook
9,0 liters (niet echt helemaal vol)
hop     procent grammen minuten 
magnum  10,2    11      60      
admiral 13,9    15      15      
strisselspaalt
         2,1    20      01
kruiden/stof    grammen minuten 
ultra moss       1      10      

gist   lallemand nottingham
10800230617711v
01-2024

dry hop
hop     procent grammen
saaz    3,3     13
refractometer          pre-boil OG
brix                   1,0275   1,033
specifiek zwartekracht 07,0     08,4

hydrometer      OG      FG
potentieel alc   4,5     0,2
suiker g/l     090       5
dichtheid     1032    1002

procent alcohol
30 = 1032 - 1002
(og - fg) * 0,136 + 0,4  = XX,X
3,868 = ( 1027,5 - 1002 ) * 0,136 + 0,4

7,9 = 8,4 - 0,5
(plato1 - plato0) * 0,544 + 0,4 = XX,X
4,6976 = ( 7,9 * 0,544 ) + 0,4

bottel
04,9l bier
45,00g suiker
014 X 33,0cl
462 cl

begin
schoonmaakwater
hevel spoelen
flessen spoelen
flessen drogen
bier overhevelen van gistingsfles
bier meten
suiker meten
suiker smelten
flessen vullen en kroonkurken
flessen markeren
afwassen

verwarming vergisting en flessen 
uren vergisting 
kWh vergisting   =(M40-L40)+J46 
uren bottelen 
kWh bottelen 
kost / kWh       € 062 
kost  =(I48+I46)*I49 
