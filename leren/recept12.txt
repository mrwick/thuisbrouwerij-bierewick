Naam: XII
smaak beschrijving: geïnfecteerd, bitter, zoet neus, diep mout

20181202 - vroege proeven
butyric infectie? te veel hops
 - tijdens bottelen was er suiker gesmolten in een ongereinigde pan
 - tijdens koelen was de koeler niet 100% onder de kook
 - wie weet wat meer?


historiek 

                   datum 
datum brouw      | 2018 10 16 mardi
dagen vergisten  | 
dagen lageren    |
warme bottel     | 2018 11 21 mercredi
koude bottel     | 2018 12 06
proef            |

                      uur  minuut  duratie  =TIJD(E10;F10;0)-TIJD(E3;F3;0) 
wegen start            09     41     6 uren 19 minuten
schroten start         09     47
maischen start         09     54
spoelen start          12     45
kook start             14     15
afkoel start           15     18
vergisting start       15     34
afwassen eind          16     00

schroten - zéér fijne geschoten
ebc    mout         grammen 
25-35  pils          650
40-50  abbey         050
15     münchen       100
25-35  tarwe         100  - bloem gemakt
totaal granen        900
ruwe rietsuiker      100
totaal vergistbaar  1000

maische
liters  3
graden  minuten (ongv)  uur  minuut
40°     10              10    04 - 10 14
52°     10              10    26 - 10 36 (12 minutes heat)
62°     45              10    51 - 11 36 (15 minutes heat)
72°     45              11    49 - 12 34 (13 minutes heat)
78°     05              12    39 - 12 44 (5 minutes heat)

liters maisch water 2
maisch g/l          450
maisch l/kg         2,22
liters spoel water  6,5
liters na spoelen   7
spoel tot brix      1,9

kook
9 liters (1,5 L water toegevoegd)
hop     procent minuten grammen  
admiral 13,9    60      12
magnum  10,2    30      16
saaz    3,3     fo      10
kruiden/stof    minuten grammen
thijm           fo       0,1
ultra moss      10       1

gist   wyeast 1762 belgian abbey II 1533101 2018 04 11

          brix  specifiek zwartekracht
pre-boil   11,4         1,044
OG         12,9         1,049
OG         13           1,050 (tweede meting)
OG         12,7         1,052 (oud refracto)
FG 

hydrometer          OG        FG      %alc 
potentieel alc       6,75      0,2
suiker g/l        1030         5
dichtheid         1050      1002      6,528
=((C41-D41)*C45)/1000 

bottel 
procent alcohol  factor  met hergisting in fles  suiker in fles 
=C45*(D35-D36)   136                             2,5ml 

verwarming vergisting en flessen 
uren vergisting 
kWh vergisting   =(M40-L40)+J46 
uren bottelen 
kWh bottelen 
kost / kWh       € 062 
kost  =(I48+I46)*I49 
