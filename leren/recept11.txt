Naam: XI
smaak beschrijving:

historiek 
per ongeluk was de vlaam aan achtergelaten
voor de 73 stap van de maisch
dat wort was weggegooid.
opnieuw begonnen om 17u

                   datum 
datum brouw        20180620
dagen vergisten 
dagen lageren 
warme bottel 
koude bottel 
proef 

                      uur  minuut  duratie  =TIJD(E10;F10;0)-TIJD(E3;F3;0) 
wegen start            17     00
schroten start         17     05
maischen start         17     22
spoelen start          19     35
kook start             20     00
afkoel start           21     40
vergisting start       22     03
afwassen eind          23     22 (met bad)

schroten
ebc    mout         grammen 
25-35  pils          701
40-50  abbey        
15     münchen       125
25-35  tarwe         125
totaal granen        950
ruwe rietsuiker      350
totaal vergistbaar  1300

maische
liters  3
graden  minuten (ongv)  uur  minuut
40°     15              17    25
62°     60              17    55
73°     20              19    02
78°     5               19    26

liters maisch water 2,5
maisch g/l          380
maisch l/kg         2,63
liters spoel water  7
liters na spoelen   9
spoel tot brix      6 (1,023)

kook
9 liters
hop     procent minuten grammen  
admiral 13,9    60      20       (wort was zéér bitter!)
magnum  10,2    50      10 
saaz    3,3      5       8
kruiden/stof    minuten grammen
thijm           fo       0,1
ultra moss      10       1

gist   white labs wlp545 belgian strong ale

          brix  specifiek zwartekracht
pre-boil  --------------DOH!-----------
OG         14,8         1,058
FG 

hydrometer          OG      FG      %alc 
potentieel alc       8       0
suiker g/l         154       0
dichtheid         1060    1000      8,16
=((C41-D41)*C45)/1000 

bottel 
procent alcohol  factor  met hergisting in fles  suiker in fles 
=C45*(D35-D36)   136                             2,5ml 

verwarming vergisting en flessen 
uren vergisting 
kWh vergisting   =(M40-L40)+J46 
uren bottelen 
kWh bottelen 
kost / kWh       € 062 
kost  =(I48+I46)*I49 
