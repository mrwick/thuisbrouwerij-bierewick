Naam:  XIV
smaak beschrijving:
proberen: een zeer blonde, zoet en niet bitere tripel

historiek 

                   datum 
datum brouw      | 2019 03 22
dagen vergisten  | 9
dagen lageren    | 3
warme bottel     | 2019 04 03
koude bottel     | 2019 04 10 (? toekomstige datum voor mij op deze moment)
proef            |

                      uur  minuut  duratie  =TIJD(E10;F10;0)-TIJD(E3;F3;0) 
wegen start            13     20
schroten start         13     30
maischen start         13     40
spoelen start          16     15
kook
           vlaam       16     58
           kook        17     27
           hop         17     40
afkoel start           18     42
vergisting start       19     00
afwassen eind          XX     XX (na bad en eten klaarmaken, 20u47)

schroten
ebc    mout               grammen 
25-35  pils                700
40-50  abbey               XXX
15     münchen             200
25-35  tarwe               100
totaal granen             1000
candico cassinade blonde   250
(van delhaize)
hygiena donkere rietsuiker 250
(van färm)
totaal vergistbaar        1500

maische
liters  3
graden  minuten (ongv)  uur  minuut
40°     05              13    51 (43°)
52°     12              14    09 - 14 21 12 graden in 14 minuten
62°     45              14    38 - 15 23 10 graden in 17 minuten
73°     30              15    34 - 16 05 11 graden in 09 minuten
78°     05              16    09 - 16 15 05 graden in 04 minuen
     01:40

liters maisch water 3,0 (vloeibaar. is dit de normaal manier of moet dat dikker zijn? - hobbybrouwers zeggen dat dit een goeie hoeveelheid is)
maisch g/l          =totaal granen / liters maisch water
maisch l/kg         =liters maisch water / (totaal granen / 1000)
maisch g/l          333 1/3
maisch l/kg         3
liters spoel water  5,5
liters na spoelen   9 (tot brim)
spoel tot brix      2,3

kook
9 liters (hoogste mogelijk)
hop     procent minuten grammen  
admiral 13,9    60      05
magnum  10,2    XX      XX
saaz    3,3     fo      03
kruiden/stof    minuten grammen
ultra moss      10       1
thijm           fo       0

gist   white labs wlp545 belgian strong ale yeast
        mfg: 2018-01-08 best gebruik voor: 2018-07-07

          brix  specifiek zwartekracht
pre-boil   10,3
OG         16,3         1,064
FG 

hydrometer          OG      FG      %alc 
potentieel alc       8,5     0
suiker g/l         165       0
dichtheid         1065    1000      8,84
=((C41-D41)*C45)/1000 

bottel 
liters van gistingsvat       5,6
flessen gevulld              16 X 33cl
suiker in flessen (priming)  40g wit
procent alcohol  factor  met hergisting in fles  suiker in fles 
=C45*(D35-D36)   136                             2,5ml 

verwarming vergisting en flessen 
uren vergisting 
kWh vergisting   =(M40-L40)+J46 
uren bottelen 
kWh bottelen 
kost / kWh       € 062 
kost  =(I48+I46)*I49 
